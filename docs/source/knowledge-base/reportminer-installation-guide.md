# ReportMiner Installation Guide

You can download the latest version of ReportMiner here: http://www.astera.com/download-center

This document details the installation of the ReportMiner client. **The server must be configured post-install in order to run any flows.**  [Click here to view this setup process.](https://astera.zendesk.com/entries/48837117-Setting-up-the-Server) 

#  

# Installing ReportMiner

 

To start the installation of ReportMiner, run setup.exe.

![mceclip0.png](reportminer-installation-guide.assets/mceclip0.png)

 

To continue with the installation, you must read and agree to the license terms.

![mceclip1.png](reportminer-installation-guide.assets/mceclip1.png)

Enter the user name and company name.  

![mceclip2.png](reportminer-installation-guide.assets/mceclip2.png)

 

Enter Destination Path. The default installation folder is **C:\Program Files (x86)\Astera Software\ReportMiner 7**.

![mceclip3.png](reportminer-installation-guide.assets/mceclip3.png)

Choose between complete or custom installation. 

![mceclip4.png](reportminer-installation-guide.assets/mceclip4.png)

 

You can customize your installation by selecting **Custom**. The following options are available for a custom installation:

 

![mceclip5.png](reportminer-installation-guide.assets/mceclip5.png)

 

This completes the configuration steps, and you are now ready to complete the installation. 

 ![mceclip6.png](reportminer-installation-guide.assets/mceclip6.png)

 

![mceclip7.png](reportminer-installation-guide.assets/mceclip7.png)

 

# Upgrading from an Earlier Version of ReportMiner

To upgrade from an earlier version of ReportMiner, you need to uninstall the previous version first.

To upgrade to a new build within the same major version, you can run the Installer for the latest build. The installer will upgrade the ReportMiner application for you keeping any previous configuration options unchanged.

 

# Starting ReportMiner

To start  ReportMiner using a Windows shortcut, navigate to

**All Programs -> Astera Software -> Report Miner 6-> Report Miner 6.**

The ReportMiner executable’s default location is

**C:\Program Files (x86)\Astera Software\Report Miner 6\ReportMinerClient.exe**

 

# Licensing ReportMiner

When you first start ReportMiner, m**ake sure to Run as Administrator.**

When you open ReportMiner, you will see the ReportMiner screen with the options to "Register" or "Try". If you still have days left on the trial period, you can select the "Try" button and continue to use the product with the trial restrictions until the trial period runs out.

 

![mceclip8.png](reportminer-installation-guide.assets/mceclip8.png)

 

If you have a license key, select the "Register" button and insert your license key in the **Serial Number** input. 

You must also enter your name, your company name, and the serial number provided to you.

Enter in the required information and click **Register**.



![mceclip9.png](reportminer-installation-guide.assets/mceclip9.png)

 

At this point, the software will contact Astera’s licensing server and register your copy.  If this not a trial key, it will also lock this key to your machine so that the key cannot be used again on another machine unless the key is deactivated via the **Deactivate License** wizard from within the product. 

If you enter a trial key, the registration screen will appear every time ReportMiner is run, showing how many days of trial are left.

 

 

# Licensing the Server Component

Licensing the Data Integrator Server (Server) is separate from the licensing of ReportMiner.

Prior to running any jobs on the Server, you must install a license using Server License Manager.

To run Server License Manager, go to:

**All Programs -> Astera Software ->  Server License Manager.**

The Server License Manager executable’s default location is

**C:\Program Files (x86)\Astera Software\Astera Integration Server 6\****ServerLicenseUI.exe**

The remaining steps of the server licensing process are similar to the client licensing described above.

Note:  The Server must be restarted in order for the new license to take effect.

![image019.png](reportminer-installation-guide.assets/image019.png)

 

## Other Licensing Issues

If you believe you have a valid license and are still having issues, try deleting the license file (.lic file) found in ReportMiner's shared data folder.  This can usually be found at

 **C:\ProgramData\Astera Software\Report Miner 6\ReportMinerClient.lic \***

 *Replace C:\ProgramData\ with C:\Documents and Settings\All Users\Application Data on older machines.

 

**Note: Because of the .lic extension, Windows may hide this file. If this is the case, uncheck the "hide operating system files" from the Windows file system options.**