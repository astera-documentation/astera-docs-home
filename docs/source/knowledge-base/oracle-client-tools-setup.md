# Oracle Client Tools Setup

This article provides instructions on how to setup the Oracle Client Tools and enable the SQL*Loader for use with Centerprise.

Prerequisites:

- Oracle Client Tools needs [Visual C++ Redistributable 2013](https://www.microsoft.com/en-us/download/details.aspx?id=40784) installed on the machine.

Steps:

1. Download the [Oracle Client Tools](http://www.astera.com/downloads/Centerprise/Additions/OracleClientTools.zip)
2. Unzip the folder to a drive that is local to where the Astera Integration Server is installed.
3. Launch the Centerprise Client and Navigate to the Server Explorer
4. Right-Click on the Server and select Configure Server ![configureserver.png](oracle-client-tools-setup.assets/configureserver.png)
5. On the Server Connect Properties screen, change the Location of the Oracle SQL*Loader to the path local to the Centerprise Server![serveroracleclienttools.png](oracle-client-tools-setup.assets/serveroracleclienttools.png)
6. Save the Server Configuration