# Replacing Licenses in ReportMiner

This article explains the step-by-step process for replacing an existing Report Miner license with a new one. This is done, for example, when a subscription is about to end.

 

# Short Version:

Delete the client and server license files found here:

 C:\ProgramData\Astera Software\Report Miner 6\ReportMinerClient.lic *

and here:

C:\ProgramData\Astera Software\Astera Integration Server 6\Centerprise6Server.lic *

*Replace C:\ProgramData\ with C:\Documents and Settings\All Users\Application Data on older machines.

Enter your new keys exactly as you did the first time.

 

# Step By Step:

## Delete the client license file

Navigate to the client license file found in the common app data folder of the machine where the client is installed. In Windows Vista and beyond, this is usually located at C:\ProgramData\Astera Software\Report Miner 6. In older machines it is located at C:\Documents and Settings\All Users\Application Data\Astera Software\Report Miner 6. In either case, delete the ReportMinerClient.lic file found there. **Note: Because of the .lic extension, Windows may hide this file. If this is the case, uncheck the "hide operating system files" from the Windows file system options.**

**![2015-02-24_1646.png](replacing-licenses-in-reportminer.assets/2015-02-24_1646.png)**

## Enter new client license key

Now that the file has been deleted, start the Report Miner client application. It will prompt you for a license key. Enter the new key given to you by the Astera sales associate. Click the "Register" button.

![rmkey.png](replacing-licenses-in-reportminer.assets/rmkey.png)

If entered correctly, you'll be greeted with the application loading and you can verify your new license key by checking the "about" box from the help menu.

![helpabout.png](replacing-licenses-in-reportminer.assets/helpabout.png)

## Delete the Server License File

Navigate to the server license file found on in the common app data folder of the machine where the server is installed. In Windows Vista and beyond, this is usually located at C:\ProgramData\Astera Software\Astera Integration Server 6. In older machines it is located at C:\Documents and Settings\All Users\Application Data\Astera Integration Server 6. In either case, delete the Centerprise6Client.lic file found there.
**Note: Because of the .lic extension, Windows may hide this file. If this is the case, uncheck the "hide operating system files" from the Windows filesystem options.**

**![server.png](replacing-licenses-in-reportminer.assets/server.png)**

## Enter the server license key

Navigate to the Server License Manager application. This application is the only way to license the server. You cannot do so through the Report Miner 6 client application.

![?name=media_1365187006077.png](replacing-licenses-in-reportminer.assets/media_1365187006077.png)

Enter the server key once prompted. Note that the server key usually starts with an "***S- prefix. Do not mix up the client and server keys. One will not unlock the other. Click the "Register" button once the key is entered.

![?name=media_1365187148713.png](replacing-licenses-in-reportminer.assets/media_1365187148713.png)

IMPORTANT!! Restart the server. Click yes when prompted. The license only takes effect after the server is restarted. Click "Yes" to restart it. If you receive an access denied error message after clicking yes, it is because you did not run this as "administrator." If this happens, simply manually restart the "Astera Integration Server" service from the Windows Control Monitor.

![?name=media_1365187311802.png](replacing-licenses-in-reportminer.assets/media_1365187311802.png)

You can now verify that the server is up and running with the correct license via the Report Miner 6 client application by clicking "View->Server Explorer" from the topmost menu. Once the "Server Explorer" is opened, click the "Information" button ( blue circle with a white "i" inside). You should now be able to see the license key you have entered, as well as any expiration date. If you can see this, the Astera Integration Server has been successfully licensed.

![?name=media_1365187554179.png](replacing-licenses-in-reportminer.assets/media_1365187554179.png)