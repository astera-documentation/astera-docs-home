# Connecting to Amazon SFTP to Access Data from an AWS Bucket

You can integrate data from an AWS bucket using **File Transfer** task option provided in Centerprise. However, to use File Transfer task to access data from your Amazon bucket, you need to have an Amazon SFTP connection set up. 

This article describes the steps to successfully use FTP task option in Centerprise to connect to an AWS bucket. 

1. Setup SFTP server endpoint at [Amazon Management Console](https://aws.amazon.com/console/) and link it with the AWS bucket. It will provide you with host URL, username and a token key file. 
2. Open a new workflow in Centerprise and drag-and-drop **File Transfer Task** object from **Toolbox >** **Workflow Tasks****.** 
   (Note: To open a new workflow go to **File > New > Workflow** or press **Ctrl +** **Shift** **+ W**)

![img](connecting-to-amazon-sftp-to-access-data-from-an-aws-bucket.assets/blobid0.png)

\3.  Double-click on the *File Transfer Task* to go to the properties. On the properties window, click on the *Ftp Connection* button in top right corner and a connection properties window will open.

![img](connecting-to-amazon-sftp-to-access-data-from-an-aws-bucket.assets/blobid1.png)

\4. Provide the details on this screen to establish the connection. Use the host URL and Login name (username) that was provided to you when you set up the SFTP server endpoint at Amazon Management Console. Select ***S******ftp*** from the drop-down list next to the *Connection Method* and use ***Port 22***. 

![img](connecting-to-amazon-sftp-to-access-data-from-an-aws-bucket.assets/blobid2.png)

\5. In the ***Authentication*** section on the same ***Web Connection*** screen, select ***Private Key*** from the drop-down list as the method to authenticate. Provide the file path of the token key provided by Amazon and click ***OK*** to save the connection properties. 

 

![img](connecting-to-amazon-sftp-to-access-data-from-an-aws-bucket.assets/blobid3.png)

\6. On the ***File Transfer Action Properties*** window, select the Ftp Action you want to perform from the drop-down list. 

![img](connecting-to-amazon-sftp-to-access-data-from-an-aws-bucket.assets/blobid4.png)

\7. Provide the source and destination paths next to **Remote Info Path** and **Local Info Path** sections and click **OK**.

![img](connecting-to-amazon-sftp-to-access-data-from-an-aws-bucket.assets/blobid5.png)

Your SFTP task is now successfully configured. You can run it to perform the desired action (Download, Upload, Rename, Remove directory, Delete file etc.) or you can add other tasks in the workflow as per your requirements. 