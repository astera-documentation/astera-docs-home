# Installing Centerprise

This document details the installation of the Centerprise client.  The server has a separate installer.  The server also requires additional set-up post install.  **The server must be configured post-install in order to run any flows.**  [Click here to view this setup process.](https://astera.zendesk.com/entries/48837117-Setting-up-the-Server) 

#  

# Installing Centerprise Data Integrator 

 

To start the installation of Centerprise 6, run setup.exe.

![Capture1.JPG](installing-centerprise.assets/Capture1.JPG)

 

To continue with the installation, you must read and agree to the license terms.

![accept2.JPG](installing-centerprise.assets/accept2.JPG)

 

Enter the user name and company name.  

![Capture3.JPG](installing-centerprise.assets/Capture3.JPG)

 

Enter Destination Path. The default installation folder is **C:\Program Files\Astera Software\Centerprise Data Integrator 6**.

![Capturemaybe.JPG](installing-centerprise.assets/Capturemaybe.JPG) 

Choose between complete or custom installation. 

![Capture4.JPG](installing-centerprise.assets/Capture4.JPG)

 

You can customize your installation by selecting **Custom**. The following options are available for a custom installation:

 

![Capturecustom.JPG](installing-centerprise.assets/Capturecustom.JPG)

 

This completes the configuration steps, and you are now ready to complete the installation. 

 ![Capture5.JPG](installing-centerprise.assets/Capture5.JPG)

 

![Capture6.JPG](installing-centerprise.assets/Capture6.JPG)

 

# Upgrading from an Earlier Version of Centerprise

To upgrade from an earlier version of Centerprise (for example, to upgrade from 5 to 6), you need to uninstall the previous version first.

To upgrade to a new build within the same major version, you can run the Installer for the latest build. The installer will upgrade the Centerprise application for you keeping any previous configuration options unchanged.

 

# Starting Centerprise

To start Centerprise Studio using a Windows shortcut, navigate to

**All Programs -> Astera Software -> Centerprise Data Integrator 6 -> Centerprise Data Integrator 6.**

The Centerprise Studio executable’s default location is

**C:\Program Files\Astera Software\Centerprise Data Integrator 6\Client\Centerprise6.exe**

 

# Licensing Centerprise Studio

When you first start Centerprise Studio, you will see the Register Centerprise Data Integrator screen. To use Centerprise Studio, you must enter either a trial key or a license key in the **Serial Number** input. 

To request a trial key, click the ‘**Don’t have a serial number?’** link and follow the instructions on the web page.

You can also register directly at http://www.astera.com/centerprise/Request-Trial-Key.aspx

You must also enter your name, your company name, and the serial number provided to you.

Enter in the required information and click **Unlock**.

![image017.png](installing-centerprise.assets/image017.png)

 

 

At this point, the software will contact Astera’s licensing server and register your copy.  If this not a trial key, it will also lock this key to your machine so that the key cannot be used again on another machine unless the key is deactivated via the **Deactivate License** wizard from within the product. 

If you enter a trial key, the registration screen will appear every time Centerprise is run, showing how many days of trial are left.

At the end of your trial period, you will be asked to register a non-trial license key..

 

 

# Licensing the Server Component

Licensing the Data Integrator Server (Server) is separate from the licensing of Centerprise Studio.

Prior to running any jobs on the Server, you must install a license using Server License Manager.

To run Server License Manager, go to:

**All Programs -> Astera Software -> Centerprise Data Integrator 6 -> Server License Manager.**

The Server License Manager executable’s default location is

**C:\Program Files\Astera Software\Centerprise Data Integrator 6\Server\****ServerLicenseUI.exe**

The remaining steps of the server licensing process are similar to the client licensing described above.

Note:  The Server must be restarted in order for the new license to take effect.

![image019.png](installing-centerprise.assets/image019.png)

 

 

# Troubleshooting License Issues

## Errors While Activating

If you get an error while registering or activating, click the **Details…** button to display the details of the issue.

![image021.png](installing-centerprise.assets/image021.png)

 

In the example shown below, activation is not allowed since the limit of activations has been reached for this license. You must deactivate the license from the original workstation and then try activating it again.

![image023.png](installing-centerprise.assets/image023.png)

 

If your license has an expiration date, you may be prompted to extend it. Please contact *support@astera.com* to request an extension code.

![image025.png](installing-centerprise.assets/image025.png)

 

## Other Licensing Issues

If you believe you have a valid license and are still having issues, try deleting the license file (.lic file) found in Centerprise's shared data folder.  This can usually be found at

***C:\Documents and Settings\All Users\Application Data\Astera Software\***

in XP and

***C:\Program Data\Astera Software\***

in Vista / Windows 7.

In this folder, expand the respective sub folder for the client or the server depending on which edition was installed. 

**Note****:** You might have to change folder settings to show hidden files.



<iframe width="560" height="315" src="https://www.youtube.com/embed/v-5vsdKw7aU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

