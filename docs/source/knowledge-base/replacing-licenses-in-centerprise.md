# Replacing Licenses in Centerprise

This article explains the step-by-step process for replacing an existing Centerprise license with a new one. This is done, for example, when a subscription is about to end.

## Short Version:

Delete the client and server license files found here:

 C:\ProgramData\Astera Software\Centerprise Data Integrator\Centerprise6Client.lic *

and here:

C:\ProgramData\Astera Software\Astera Integration Server \Centerprise6Server.lic *

*Replace C:\ProgramData\ with C:\Documents and Settings\All Users\Application Data on older machines.

Enter your new keys exactly as you did the first time.

**Note: Because of the .lic extension, Windows may hide this ProgramData folder. If this is the case, check the "show hidden files and folders" and uncheck the "hide operating system files" from the Windows file system options.**

## Step By Step:

## Deactivate the license if it will still be used (optional)

![?name=media_1365185018031.png](replacing-licenses-in-centerprise.assets/media_1365185018031.png)

If you plan on moving this key to another machine, make sure you run the "Deactivate License" command. If you do not do this, the next person who tries to activate this license key number will receive a "maximum number of activations reached" error. If this license has expired or will be expiring soon, you can skip this step.

![?name=media_1365185272095.png](replacing-licenses-in-centerprise.assets/media_1365185272095.png)

## Stop Centerprise

Exit from the client before changing any license file.

![?name=media_1365185488717.png](replacing-licenses-in-centerprise.assets/media_1365185488717.png)

Open the Windows Service Control Monitor by typing "services" in the search menu.

![?name=media_1365185736352.png](replacing-licenses-in-centerprise.assets/media_1365185736352.png)

Choose the "Component Services" entry.

![?name=media_1365185845804.png](replacing-licenses-in-centerprise.assets/media_1365185845804.png)

Stop the "Astera Integration Server" service.

![?name=media_1365185972165.png](replacing-licenses-in-centerprise.assets/media_1365185972165.png)

## Delete the client license file

Navigate to the client license file found in the common app data folder of the machine where the client is installed. In Windows Vista and beyond, this is usually located at C:\ProgramData\Astera Software\Centerprise Data Integrator. In older machines it is located at C:\Documents and Settings\All Users\Application Data\Astera Software\Centerprise Data Integrator. In either case, delete the Centerprise5Client.lic file found there. **Note: Because of the .lic extension, Windows may hide this file. If this is the case, uncheck the "hide operating system files" from the Windows file system options.**

![?name=media_1365186195742.png](replacing-licenses-in-centerprise.assets/media_1365186195742.png)

## Delete the Server License File

Navigate to the server license file found on in the common app data folder of the machine where the server is installed. In Windows Vista and beyond, this is usually located at C:\ProgramData\Astera Software\Astera Integration Server 6. In older machines it is located at C:\Documents and Settings\All Users\Application Data\Astera Integration Server 6. In either case, delete the Centerprise6Client.lic file found there.
**Note: Because of the .lic extension, Windows may hide this file. If this is the case, uncheck the "hide operating system files" from the Windows filesystem options.**

![?name=media_1365186481866.png](replacing-licenses-in-centerprise.assets/media_1365186481866.png)

## Enter new client license key

Now that the file has been deleted, start the Centerprise Data Integrator client application. It will prompt you for a license key. Enter the new key given to you by the Astera sales associate. Click the "Unlock" button.

![?name=media_1365186759251.png](replacing-licenses-in-centerprise.assets/media_1365186759251.png)

If entered correctly, you'll be greeted with the application loading and you can verify your new license key by checking the "about" box from the help menu.

![?name=media_1365186885535.png](replacing-licenses-in-centerprise.assets/media_1365186885535.png)

## Enter the server license key

Navigate to the Server License Manager application. This application is the only way to license the server. You cannot do so through the Centerprise Data Integrator client application.

![?name=media_1365187006077.png](replacing-licenses-in-centerprise.assets/media_1365187006077.png)

Enter the server key once prompted. Note that the server key usually starts with an "**S- prefix. Do not mix up the client and server keys. One will not unlock the other. Click the "Register" button once the key is entered.

![?name=media_1365187148713.png](replacing-licenses-in-centerprise.assets/media_1365187148713.png)

IMPORTANT!! Restart the server. Click yes when prompted. The license only takes effect after the server is restarted. Click "Yes" to restart it. If you receive an access denied error message after clicking yes, it is because you did not run this as "administrator." If this happens, simply manually restart the "Astera Integration Server" service from the Windows Control Monitor.

![?name=media_1365187311802.png](replacing-licenses-in-centerprise.assets/media_1365187311802.png)

You can now verify that the server is up and running with the correct license via the Centerprise Data Integrator client application by clicking "View->Server Explorer" from the topmost menu. Once the "Server Explorer" is opened, click the "Information" button ( blue circle with a white "i" inside). You should now be able to see the license key you have entered, as well as any expiration date. If you can see this, the Astera Integration Server has been successfully licensed.

![?name=media_1365187554179.png](replacing-licenses-in-centerprise.assets/media_1365187554179.png)

 

- [media_1365185018031.png](https://astera.zendesk.com/attachments/token/95xbqp0xdezs2kx/?name=media_1365185018031.png)
- [media_1365185272095.png](https://astera.zendesk.com/attachments/token/tyxlutoitvve5ml/?name=media_1365185272095.png)
- [media_1365185488717.png](https://astera.zendesk.com/attachments/token/ue3ffxiig8ggfvh/?name=media_1365185488717.png)
- [media_1365185736352.png](https://astera.zendesk.com/attachments/token/5ctz7hmenfoohzk/?name=media_1365185736352.png)
- [media_1365185845804.png](https://astera.zendesk.com/attachments/token/ongnugskx97d4pa/?name=media_1365185845804.png)
- [media_1365185972165.png](https://astera.zendesk.com/attachments/token/klyo8uyls8n2av4/?name=media_1365185972165.png)
- [media_1365186195742.png](https://astera.zendesk.com/attachments/token/wy2opa6ymvo2jat/?name=media_1365186195742.png)
- [media_1365186481866.png](https://astera.zendesk.com/attachments/token/oo4vidpm6uy0oci/?name=media_1365186481866.png)
- [media_1365186759251.png](https://astera.zendesk.com/attachments/token/nnvgvlcllhxsgnl/?name=media_1365186759251.png)
- [media_1365186885535.png](https://astera.zendesk.com/attachments/token/cqcefvcznsw34ji/?name=media_1365186885535.png)
- [media_1365187006077.png](https://astera.zendesk.com/attachments/token/qoxawjqbvrfjcgu/?name=media_1365187006077.png)
- [media_1365187148713.png](https://astera.zendesk.com/attachments/token/g3iejubkdfsolrq/?name=media_1365187148713.png)
- [media_1365187311802.png](https://astera.zendesk.com/attachments/token/3uilgrnibv0uw7s/?name=media_1365187311802.png)
- [media_1365187554179.png](https://astera.zendesk.com/attachments/token/takvbd2sjbmt5pw/?name=media_1365187554179.png)

- [media_1365186481866.png](https://astera.zendesk.com/hc/en-us/article_attachments/115001250523/media_1365186481866.png) (80 KB)
- [media_1365186759251.png](https://astera.zendesk.com/hc/en-us/article_attachments/115001250543/media_1365186759251.png) (70 KB)
- [media_1365187554179.png](https://astera.zendesk.com/hc/en-us/article_attachments/115001250563/media_1365187554179.png) (30 KB)
- [media_1365185018031.png](https://astera.zendesk.com/hc/en-us/article_attachments/115001250583/media_1365185018031.png) (40 KB)
- [media_1365185272095.png](https://astera.zendesk.com/hc/en-us/article_attachments/115001250603/media_1365185272095.png) (100 KB)
- [media_1365185972165.png](https://astera.zendesk.com/hc/en-us/article_attachments/115001250623/media_1365185972165.png) (200 KB)
- [media_1365186885535.png](https://astera.zendesk.com/hc/en-us/article_attachments/115001250643/media_1365186885535.png) (50 KB)
- [media_1365185736352.png](https://astera.zendesk.com/hc/en-us/article_attachments/115001250663/media_1365185736352.png) (20 KB)
- [media_1365186195742.png](https://astera.zendesk.com/hc/en-us/article_attachments/115001250683/media_1365186195742.png) (60 KB)
- [media_1365187148713.png](https://astera.zendesk.com/hc/en-us/article_attachments/115001250703/media_1365187148713.png) (80 KB)
- [media_1365187311802.png](https://astera.zendesk.com/hc/en-us/article_attachments/115001250723/media_1365187311802.png) (20 KB)
- [media_1365185488717.png](https://astera.zendesk.com/hc/en-us/article_attachments/115001250743/media_1365185488717.png) (50 KB)
- [media_1365185845804.png](https://astera.zendesk.com/hc/en-us/article_attachments/115001250763/media_1365185845804.png) (40 KB)
- [media_1365187006077.png](https://astera.zendesk.com/hc/en-us/article_attachments/115001250783/media_1365187006077.png) (40 KB)