# POSTing Data Using the REST Client

The Centerprise REST client component can be used to POST data to web servers.  However, there are some inherent axioms when it comes to the way Centerprise maps hierarchical data that may present some challenges when trying to compose a JSON payload to the web server.  This document will list  the most common challenges and concepts and how to deal with them in Centerprise.

## Cardinality

The issue of cardinality is probably the biggest issue facing a Centerprise developer when using the REST component.  Depending on what the mapping into the data structure under the *Input* node looks like, you could end up with a completely different payload being sent to the web server.  For example, take a look at the following  screen capture:

![2016-03-02_2323.png](posting-data-using-the-rest-client.assets/2016-03-02_2323.png)

This example shows an xml file with 3 SalesOrder nodes being mapped into the item collection under the *Input* node of the RestClient1 action.  A preview of the Passthru showing just the orders is below:

![2016-03-02_2335.png](posting-data-using-the-rest-client.assets/2016-03-02_2335.png)

And here is the preview of the union of all line items under all orders;

![2016-03-02_2336.png](posting-data-using-the-rest-client.assets/2016-03-02_2336.png)

Notice that there are 3 orders and a total of 9 line items.  Actually, there are 3 line items for every order.  And in fact, we would like to make a grand total of 3 REST POSTs consisting of all line items for a particular order.  However, the way we have the mapping set up, we end up with ***9*** separate REST requests to the web server.![2016-03-02_2340.png](posting-data-using-the-rest-client.assets/2016-03-02_2340.png)

Now watch what happens when we map something to the URL parameter.  We now get 3 POSTs to the server.  Each with a *collection* of line items instead of just one.  ![2016-03-02_2345.png](posting-data-using-the-rest-client.assets/2016-03-02_2345.png)

Why does this happen?  This is because when mapping into a tree, the source tree will pass along the top most node mapped as the "record".  When we were only mapping from line item collection, we had 9 records.  When we included maps from the order, we suddenly had 3 records.  So you should have a "parent" record for each REST request you wish to make. 

So what if your source was a flat structure such as an Excel workbook or database table?  In that case, you will create a REST request for every row in the source grid.  To get around this, you must make a tree in order to create desire JSON or XML data.  A good way to do this would be to use the Tree Join transformation.

 

## Rootless JSON Arrays

By default Centerprise will treat XML and JSON pretty much exactly the same. This is because they are very similar and, in fact, we use the same writer technology in order to form JSON  or XML.  However, there some differences that arise which highlight some XML rules that limit the kind of JSON that can created for a REST request.  The most obvious of these rules is the one that, for XML, states that you can only have a single root object in XML.  This is not the case for JSON.  For example, look at the following JSON:

![2016-03-03_0008.png](posting-data-using-the-rest-client.assets/2016-03-03_0008.png)

Notice that this is javascript array with no root.  This is legal JSON, but notice the resultant tree structure that is created.  It has a phantom "'root" object in the tree.  This will produce JSON that will look slightly different that the sample JSON used the build the layout.  In order to generate the rootless JSON array desired here, you must check the "Omit Root If JSON".  Note that this will not alter the tree at all.  This option will only surface when the REST payload is being created.