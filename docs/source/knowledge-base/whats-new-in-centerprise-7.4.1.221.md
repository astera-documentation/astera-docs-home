# What’s New in Centerprise 7.4.1.221

## Server Stability

#### Improved Recovery of the Server

With improved server recovery, Astera Server will no longer enter a permanent error state after a repository database outage event. Instead, it will recover as soon as the connection is restored. The server can now survive most repository database outages without any user action required.

#### New and Improved Error Logging

The logging of repository connection issues, as well as general server health info logging, has been greatly improved. Whenever the repository database is not available, the Server will write any repository connection issues in the Windows event log and include a link to the Error file for easier troubleshooting. An information entry is also added to the server log when the connection is restored.

### Changes since 7.4 April Release:

- Auto Purge Log feature has been redesigned to run during times when the server is idle. This has shown to improve reliability of the purge process since it minimizes the possibility of deadlocks with other running processes.
- A new server diagnostics feature has been added, making it easier to troubleshoot issues in a customer’s environment. This new feature is invoked with a simple right click on the Server to show Server Information window.  A detailed diagnostics file is generated in XML format.  This file can then be securely transmitted over to Astera to allow for quick and efficient analysis. This new design makes it possible to identify and resolve certain types of customer inquiries faster.
- Server job termination logic has been improved.

## Oracle database support

- Centerprise 7.4 April Release introduces a new way of bulk writing data to Oracle tables using Oracle’s SQL*Loader tool. SQL*Loader is a reliable and fast way to load large data sets to an Oracle table.   This new data load option is available in addition to Single Record Insert, Array Insert, and Bulk Insert, all previously available for Oracle destinations. In addition to being extremely reliable, SQL*Loader has shown to handle concurrent loading, sequence objects, identity columns, indexes and record level logging, among other things.
- Oracle Diff Processor allows the user to specify the data load option to be used when writing to the Diff table (the user can choose between Array Insert or Bulk Insert).

## Support for Visual Studio Team Services

Centerprise 7.4 can connect to Team Foundation Version Control services available through Visual Studio online using the following new authentication schemes, in addition to regular Windows authentication:

- Personal Access Token
- Alternate Authentication
- Rest Authentication

Unicode character support in expressions - Unicode characters are now supported in expressions, calculation formulas and filter rules anywhere in the product. 

A new ‘Update Current Version’ field type for SCD objects – A new field type is introduced for the Slowly Changing Dimensions write strategy.  This field type is called SCD1+.  It is similar to SCD1 type, but it only updates the current version of the record instead of all versions.  This is useful when the SCD table has a mix of SCD1 and SCD2 fields and the user wants to update only the current version of the record with the new SCD1 value.

FTPList Source object adds option to iterate through subdirectories – We have added an option in FTPList object properties to return an entry for directory and also optionally iterate through sub directories.  

## COBOL reader improvements

You can specify COBOL Picture and COBOL Usage fields when reading COBOL files using the Fixed-Length Source object.   We have also added a UI option in the COBOL Source object to enable signed zone decimals, which allows the user to specify whether the sign takes its own character position in the ASCII stream.   This should help improve compatibility with various representations of signed decimals when reading COBOL mainframe originating data encoded in ASCII.

A new Distinct Consolidated View in Replace Parameter Info tool

We have added a new Distinct Consolidated view in Replace Parameter Info screen in Project Explorer and also on the flow toolbar.  This new view scans the file paths on the flow diagram and/or within the project, and then it outputs a single consolidated entry for those file paths that have a common outer folder.  This allows the user to quickly find and replace those common parts of the file path with a new target folder while keeping the subfolder structure ‘as is’.  This makes it easier to quickly migrate complex projects across environments.

## Miscellaneous updates

- Updated Name and Address Parser database files for US based addresses
- Tfs2015 support is included with the installer
- Improved the parsing and building of EDIFact messages

## Miscellaneous fixes:

- Pushdown (ELT) fixes
- Fixes in Server Administration
- Salesforce bulk upsert fix
- Oracle bulk update fixes
- Spaces in MSSQL objects are supported
- Support for multiple Variable objects on the flow diagram is re-introduced based on customer demand

 