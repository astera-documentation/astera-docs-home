# Running Microsoft Access Database Engine with Centerprise

If you have a 32-bit Centerprise running on a machine and would like to use the Microsoft Access Database Engine provider as a data source, you would need to have the corresponding 32-bit version of it installed on the machine as well. The problem arises when a 64-bit version of Microsoft Access Database Engine is already installed on the machine, and a 32-bit version is needed by Centerprise. Microsoft does not permit installation of both 32-bit and 64-bit by default, but it can be done with a workaround. 

 

### Microsoft has released the installers here: 

https://www.microsoft.com/en-us/download/details.aspx?id=13255 

 

To install the driver required you would need to run it with the "/passive" argument specified. 

If you have a 32-bit Centerprise and have a 64-bit Microsoft Access driver installed, you would need to install the 32-bit driver version: 

AccessDatabaseEngine.exe /passive 

 

If you have a 64-bit Centerprise and have a 32-bit Microsoft Access driver installed, you would need to install the 64-bit driver version: 

AccessDatabaseEngine_X64.exe /passive

 

Please try uninstalling either version (e.g. 32 bit or 64 bit) after you have both versions installed.  You will get into an error like one below. 

Note that you will have this error from either version (e.g. 32 bit and 64 bit) when both versions are installed.

 

![img](https://outlook.office.com/owa/service.svc/s/GetFileAttachment?id=AAMkADBkMjBkZmVjLWZkZDItNGU4OC05ZTdjLWE4Zjg4ZTVhMDY4NwBGAAAAAAB2%2BhjpfRjORKhxKkDjXdc8BwBlYj8QOFrHSqUEqJ9jNU%2B%2BAAAAAAEMAABlYj8QOFrHSqUEqJ9jNU%2B%2BAAE0GBeWAAABEgAQAFjh8vZ5gU5GlzCcR0i%2Fi8k%3D&X-OWA-CANARY=7U51s5yjP0GbWW6cel5ZFFCgIuK989MY7UvXKcM_HaTEYIADgqx3RP6U4fUD3TQdDbGIDDwB340.)

 

The workaround is to run the setup with the /extract switch first.  This extracts it to a temp folder.  Then right-click on it and select ‘Uninstall’ from the context menu.   

 