# Parameterization

Centerprise gives the option to parameterize some of the values for an object in a flow which allows the values to be replaced during runtime. Parameterized values in the Config Parameters are typically the most commonly changed values and the availability of these parameters vary depending on the object. If an object supports Parameterization, a Config Parameters Wizard Page will be shown for that object.

![configParametersSample.png](parameterization.assets/configParametersSample.png)

Parameters can provide easier deployment of flows by eliminating hardcoded values, and provide an easier way of changing multiple configurations with a simple value change.

Parameters can be changed in the Config Parameters wizard page. Parameters left blank will use their default values assigned on the properties page.

## Parameter Values

Values assigned to parameters can be a string representation of the allowed value type or a variable denoted with the $ symbol surrounded by open and closed parenthesis. Variables inside parameters are replaced during runtime with their assigned values denoted in a Variables object or Config File.

Variables on a flow can be declared locally by using a Variables object or project-wide by using a Config File. The Config Parameters screen replaces any string with the form of:

**$(<variable name>)**

A combination of variables and/or values can be applied to the parameters, for example:

**C:\Files\$(gFile)**

**$(gDirectory)$(gFile)**

## Config File

Config Files are used in projects to store project-wide variables and their values. These variables are Name-Value pairs with string representations of values that can be used in the Config Parameters. Config Files can be created in Centerprise from the main menu, or added from the project menu.

![configFileSample.png](parameterization.assets/configFileSample.png)

Config Files are best used inside a project. Any files opened within Centerprise will have access to the variables inside the set Config File of the current Project opened.

A Config File needs a Name Value pair. Names accept Alphanumeric characters and the '_' (underscore) symbol. The Values are string representations of values to be assigned.

## Deployment

The Centerprise Scheduler provides an option to deploy a project with an optional Config File. For more information about deployment see [Project Deployment](https://astera.zendesk.com/hc/en-us/articles/360000911193-Project-Deployment).

## Example

### Parametrizing the Source File Path

File Path for Delimited Sources can be parameterized enabling it to be changed during runtime and whenever necessary. This gives the ability to have the Source File Path be declared independently from the flow. A way to set the parameters is to declare the path in a variable object.

![ex1variables.png](parameterization.assets/ex1variables.png)

The flow can use the variables as values for the Config Parameters for any object contained within the flow.

![ex1flow.png](parameterization.assets/ex1flow.png)

For this example, we have a simple flow that transfers data from a delimited source to a delimited destination. The FilePath for a Delimited File Source can be modified using parameters in the Config Parameters screen.

![ex1configvar.png](parameterization.assets/ex1configvar.png)

The values entered for FilePath will be changed during runtime. The job trace will reflect the change.

![ex1resolve.png](parameterization.assets/ex1resolve.png)

To use a Config File as a source for the parameter, we need to create a project and add a Config File along with the Dataflow.

![ex1project.png](parameterization.assets/ex1project.png)

The Config File behaves similarly with the Variables object by supplying a Name and a Value.

![configFileSample.png](https://astera.zendesk.com/hc/article_attachments/360001045334/configFileSample.png)

The Name can then be used inside the Config Parameters screen.

![ex1configvar2.png](parameterization.assets/ex1configvar2.png)