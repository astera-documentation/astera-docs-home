# System Requirements

# System Requirements

Please ensure that your system meets the minimum requirements listed below before you begin installing the application.

 

| Application Requirements                        |                                                              |                    |
| ----------------------------------------------- | ------------------------------------------------------------ | ------------------ |
| Client Application Processor                    | Single Core                                                  | 3.0 GHz or greater |
| Multi Core                                      | 2.0 GHz or greater                                           |                    |
| Operating System                                | Microsoft Windows XP/2003/Vista/Windows 7 Platforms          |                    |
| Memory                                          | 4 GB, 8 GB or greater recommended                            |                    |
| Hard Disk Space                                 | 100 MB – if .NET Framework is preinstalled                   |                    |
| 250 MB – if .NET Framework is not yet installed |                                                              |                    |
| Other                                           | Requires Microsoft .NET Framework 4.6, SQL Server (SQL Server Express is ok) SQL Server 2005 and up |                    |
| Server Application Processor                    | Dual Core Processor rated at 2.0 GHz or higherQuad Core Processor highly recommended |                    |

 

Note:    The overall speed and performance of the application depend on the configuration of your desktop computer. More memory and higher processing speed on the system will result in faster performance, especially when transferring large amounts of data as the application takes advantage of the multicore hardware to parallelize operations.