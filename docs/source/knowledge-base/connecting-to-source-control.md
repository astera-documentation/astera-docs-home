# Connecting to Source Control

Centerprise features integration with Team Foundation Server and VSTS to manage version control over your projects.

To connect to a Source Control server, navigate to

Project > Source Control > Connect to Source Control Server

![tfsconnect.png](connecting-to-source-control.assets/tfsconnect.png)

## ![tfsConnectDialog.png](connecting-to-source-control.assets/tfsConnectDialog.png)

## Connecting to TFS

**Windows Authentication** can be used to connect to a Team Foundation Server. This uses the current user which is logged on to Windows and uses the windows credentials to authenticate the user.

## Connecting to VSTS

Various authentication methods can be used to connect to Visual Studio Team Services (Visual Studio Online). When using these authentication methods, the **Path** can be set as empty with **Port** set to 0.

**Alternate Authentication** can be enabled inside the Security settings for your Visual Studio account. This enables the account to have a manageable separate username and password.

![tfsalt.png](connecting-to-source-control.assets/tfsalt.png)

**Personal Access Tokens** can also be enabled and uses a generated token to authenticate the account.

![tfsPat.png](connecting-to-source-control.assets/tfsPat.png)

**Rest Authentication** provides a built in prompt to login using your Microsoft account.

## Auto Connect

By default, Centerprise will try to auto-connect to source control once the client starts. This can be changed by Navigating to

Tools > Options > Source Control Tab

![tfsoptions.png](connecting-to-source-control.assets/tfsoptions.png)