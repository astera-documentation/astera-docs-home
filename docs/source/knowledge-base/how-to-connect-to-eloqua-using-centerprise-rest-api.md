# How to Connect to Eloqua using Centerprise REST API

Follow the steps below to connect to Eloqua using Centerprise REST API:

1. Drag-and-drop the *REST Client* object on the designer from the *Services*section in the toolbox.

**Note:** If you are unable to see the Toolbox, go to *View > Toolbox* or press *Ctrl + Alt + X*.

![mceclip0.png](how-to-connect-to-eloqua-using-centerprise-rest-api.assets/mceclip0.png)

\2. Double-click on the *REST Client* object to go the *Properties*

*![mceclip1.png](how-to-connect-to-eloqua-using-centerprise-rest-api.assets/mceclip1.png)*

*![mceclip2.png](how-to-connect-to-eloqua-using-centerprise-rest-api.assets/mceclip2.png)*

*3.* In the *REST Request* section, specify the *HTTP Method* (GET, PUT, POST, DELETE) and specify the *Base URL.* In this case, it will be the base url used by Eloqua ([https://login.eloqua.com](https://login.eloqua.com/)).

\4. Provide the resource/API endpoint you want to work with in the space provided next to the *Resource* section.

![mceclip3.png](how-to-connect-to-eloqua-using-centerprise-rest-api.assets/mceclip3.png)

\5. Next, change the *Authentication* to *Basic*. It is set to *None* by default. Provide *User Id* (this is the ID that you have used to create the account on Eloqua), *Password* and *Security Token*.

**Note:** User Id will be in the following format:

​                       *CompanyName/UserName (For example: AsteraSoftware/HassanAhsan)*

*![mceclip4.png](how-to-connect-to-eloqua-using-centerprise-rest-api.assets/mceclip4.png)*

Following the steps mentioned in this article, you will be able to connect to Oracle Eloqua Marketing Cloud successfully using Centerprise REST client.

 