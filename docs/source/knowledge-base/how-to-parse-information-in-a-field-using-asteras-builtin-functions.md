# How to parse information in a field using Astera’s built-in functions

Astera Centerprise has an extensive library of built-in functions that make common data transformation cases incredibly easier and faster. In this article, we will discuss two common cases where users have to parse information contained in a single field into multiple fields.

## Case 1: Parse Contact Names

Having a *Name* field in a data set is very common. Imagine a case where you have *Full Name* stored in a single data field and you want to separate the first name from the last name. You can easily accomplish the task using a built-in function in Centerprise.

1. Go to *Toolbox > Function Transformations > Name and Address > ParseName.*

*![mceclip0.png](how-to-parse-information-in-a-field-using-asteras-builtin-functions.assets/mceclip0.png)*

\2. Drag-and-drop the *ParseName* object on the designer.

\3. Expand the *Input* node and map the relevant field from the source dataset to the *name* field in the *Input*

*![mceclip1.png](how-to-parse-information-in-a-field-using-asteras-builtin-functions.assets/mceclip1.png)*

\4. Now if you preview the output, you’ll be able to see that Centerprise has parsed the information into separate output fields. So, now you have a distinct field for first name, middle name, last name and so on.

![mceclip2.png](how-to-parse-information-in-a-field-using-asteras-builtin-functions.assets/mceclip2.png)

\5. You can map these output fields to other objects in the dataflow as required.

## Case 2: Parse String Separated by a Delimiter

If you have an input field that contains distinct pieces of information separated by a delimiter, you can easily parse the information using *SplitString* function in Centerprise.

A *SplitString* function takes the string and the separator information as an input.

1. Go to *Toolbox > Function Transformation > String > SplitString.*
2. Drag-and-drop the *SplitString* object on the designer.

![mceclip3.png](how-to-parse-information-in-a-field-using-asteras-builtin-functions.assets/mceclip3.png)

\3. Expand the input node and you’ll be able to see two inputs – *str*(string) and *separator* (delimiter/separator used to separate the information). Map the relevant fields to the input node.

![mceclip4.png](how-to-parse-information-in-a-field-using-asteras-builtin-functions.assets/mceclip4.png)

\4. Now if you preview the output, you should be able to see the information split in various parts.

![mceclip5.png](how-to-parse-information-in-a-field-using-asteras-builtin-functions.assets/mceclip5.png)