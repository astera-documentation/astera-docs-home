# Project Deployment

Deployment is a way for setting up Centerprise Projects to run on the Scheduler. Deployment enables the use of a Config File on a Project Archive (*.car) file, making the selected Flow run independent of any local parameters.

![deploymentcommand.png](project-deployment.assets/deploymentcommand.png) 

Managing Deployments for a Cluster is done by selecting Deployments from the Cluster menu inside the Server Explorer tab. A Deployment is made up of a project .car file with an optional Config File for parameters. You can set up multiple Deployments with different configurations on the cluster.

## Setup

The Deployment Directory for the cluster needs to be specified in the cluster settings.

![deploymentdirectory.png](project-deployment.assets/deploymentdirectory.png) 

The Deployment Directory is the location where the Deployment files will be stored for the server to access when a Deployment schedule is run.

![deploymentlist.png](project-deployment.assets/deploymentlist.png) 

Multiple Deployments can be configured on the Cluster on the Deployment Window. The top grid lists all of the Deployment Files configured. The bottom is the configuration for a new Deployment or the currently selected one.

![deploymentbuttons.png](project-deployment.assets/deploymentbuttons.png) 

Adding a new Deployment is done by clicking Add Deployment. Clicking Save will save a new Deployment or any changes made to the current Deployment selected. Clicking Delete will delete the currently selected Deployment.

![deploymentsample.png](project-deployment.assets/deploymentsample.png) 

A Deployment can be configured with the following settings:

- Name – Uniquely identifies a Deployment for use on the Scheduler
- Archive File Path – An archived copy of the Project file (*.car)
- Config File Path – an optional file that can be used to specify parameters for the project. For more information about the Config File see [Parameterization](https://astera.zendesk.com/hc/en-us/articles/360000926754-Parameterization).
- Comment – a comment for use in the Deployment list.

## Scheduling

The Scheduler provides the ability to run a Deployment by selecting Deployment on the Schedule Type.

![deploymentschedulersample.png](project-deployment.assets/deploymentschedulersample.png) 

The Deployment can be selected on the drop down menu. The Start Item specifies the file that will run when the schedule gets triggered.