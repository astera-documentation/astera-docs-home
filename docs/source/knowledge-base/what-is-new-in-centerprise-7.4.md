# What is New in Centerprise 7.4

## New Project Deployment Model with Config Parameters

- 4 introduces a new way the flows can be deployed to the target environment, which streamlines the management of environment-specific database connections, network paths and flow variables. A project can be packaged into a single deployment file (aka CAR file), and the deployment file can have several config files with environment specific settings, making it possible to reuse the same deployment file and customize it for each target environment.
- Scheduled jobs can be created from the deployment file. The Scheduler allows the user to select the flow from a deployment file and optionally specify a config file that will modify the flow’s parameters as needed for the target environment.
- Most objects on the flow diagram have a new ‘Config Parameters’ properties screen. This is where the user can specify the parameters that should be assigned values from the config file. The config parameters override the corresponding property values when the deployed flow runs.

## Database Writer Optimizations

- Database write speeds are faster now for most database providers. The speed gain is especially noticeable for larger data sets using bulk or array insert.   This performance improvement applies to several key database providers, including MSSQL, DB2, Oracle, Salesforce, MySQL, and Postgres.
- Database writer framework has been redesigned to use temp tables with indexes on key columns, as well as temp files where applicable, which reduces network and database overhead, and increases bulk write speed
- New implementation of bulk Upsert (Update followed by Insert) for most database providers. This brings a significant speed advantage over previous versions, which processed data loads on a per record basis
- Array Insert is faster than previous versions and it also optimizes memory use for typical batch sizes

## Server Stability and Performance Optimizations:

- Flow termination logic has been improved. Flows terminate faster now, and the Server is generally more stable after terminating flows, even ones that involve termination of child processes and/or database transaction rollback
- Server framework has been updated to reduce the possibility for resource leaks, deadlocks and orphan connections, which further improves server stability
- Job Monitoring UI is much faster now

## Client Performance Optimizations:

- Object trees refresh faster on the flow diagram
- Scrolling of objects with complex trees is faster
- Project load and refresh times are significantly faster for larger projects. In addition, lineage building can be temporarily disabled in Project Explorer, which helps optimize project tree response times

## New Transformations and Transformation Features:

- **Reconcile** transformation - a new Transformation object on the toolbox.  This transformation makes it possible to compare two streams and capture their differences.  This transformation can also output differences between nodes in complex trees.
- A new **Transform Layout** interface, which greatly simplifies the design and management of collections in complex object trees. This new feature is invoked via a command on the context menu for EDI Parser and EDI Builder objects, as well as the PassThru transformation. Using this new feature, the user can select a node and transform it to modify the structure of the object tree without the need to update the underlying schema.  The user can show the node ‘as is’, hide it from the tree, or switch between a collection or single instance presentation on the tree. The following new features are available for managing collections:
- **Flatten with Item Count** feature creates a target number of single instance nodes from a single collection. These nodes can then be mapped one at a time to their own downstream objects. 
- **Flatten Based on Rules** acts in a similar fashion, but it routes the data to a target single instance node based on some rule criteria
- **Merge with Item Count** feature does the opposite of the two flattening features described above. It multiplexes any number of individually mapped single instances into a single Collection.

The new interface makes working with complex trees easier, which can be an advantage when processing EDI structures. In addition, the Transform Layout interface makes it possible to specify custom filter criteria or Sort orders to further control how data should be processed.

## New Database Features:

- **Identity Columns** and **Sequence Objects** are supported in more database providers now. 7.4 introduces support of identity columns in Oracle destinations (this requires Oracle 12c or later) as well as adds more robust handling of sequence objects in the database providers that support it, including Oracle, DB2 and Postgres.
- Improved handling of binary data in DB2 and Oracle
- Postgres destination has a new mode for bulk loading data from a temp file directly into the Postgres server. This increases write speeds for certain configurations.

## Other New Features:

- **Importing Schedules and Deployments** – schedules and deployments can now be exported into a file, and then imported into a target repository. This simplifies repository migration tasks as it saves the need to backup/restore Centerprise repository.
- **FileSystem** object has a new property to return an extra record for each folder
- **EmailSource** object supports POP3 protocol now, in addition to IMAP. Email logging has been improved.
- **Write To** command on the context menu lets you add a destination object of your choice and map it in one step

## New EDI Features

- Improved X12, HL7 and Edifact support
- EDI repositories for X12, HL7 and Edifact are no longer bundled with the client/server installer. They can be installed as needed by selecting the required dialect in the special Addon installer.
- EDI transaction configuration has been simplified. In 7.4, EDI versions and transaction sets can be selected directly in the EDI object on the diagram, rather than via the Trade Partner Profile dialog.

## New ReportMiner Features:

- **Improved portability of Report Models across different locales.** Report Models can now be re-used in different locales without worrying about date/currency/number format compatibility in the target locale. The user can specify that the report model keeps the settings of the locale in which the model was created.  These settings will apply regardless of the target environment/locale where the report model is used. Conversely, the report model can be set to adapt to the target locale so that the locale-specific settings are processed according to the target environment.

 