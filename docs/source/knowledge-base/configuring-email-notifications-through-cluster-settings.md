# Configuring Email Notifications through Cluster Settings

In Centerprise, you can opt to send out email notifications at the start and/or completion of a scheduled task, such as a job run or file drop. If you have enabled Notification Email on the Scheduler, you need to configure the mail setup through Server Cluster Settings to be able to send and receive notification emails.

 

### **Steps to Set Up Email Notifications through Cluster Settings**

1. Go to the *Server* menu in the menu bar located at the top of your screen and choose *Server Explorer* from the list.

 ![1.png](configuring-email-notifications-through-cluster-settings.assets/1.png)

This will open the *Server Explorer* pane in Centerprise. Here, you will find all the servers you are connected to, listed under the *Server Connections* root node.

 ![2.png](configuring-email-notifications-through-cluster-settings.assets/2.png)

 

1. Right-click on the *DEFAULT* server node and click on *Cluster Settings* from the list.

 ![3.png](configuring-email-notifications-through-cluster-settings.assets/3.png)

This will open the *ClusterSettings* window in Centerprise.

![5.png](configuring-email-notifications-through-cluster-settings.assets/5.png)

 There are three tabs – General, User Admin, and Mail Setup tab – on the *ClusterSettings* window.

![6.png](configuring-email-notifications-through-cluster-settings.assets/6.png)

 

1. Go to the *Mail Setup* tab. Here, you have to provide your credentials, and other information to configure email notifications.

 ![7.png](configuring-email-notifications-through-cluster-settings.assets/7.png)

 

1. In the *Sender Address* field, type in the email ID you want to send the notifications from.

 ![8.png](configuring-email-notifications-through-cluster-settings.assets/8.png)

 

1. Next, provide the name of Host, that is, the name of the domain that hosts your email servers.

![9.png](configuring-email-notifications-through-cluster-settings.assets/9.png)

 

1. Now, provide the port number. Port Number depends on the SMTP settings of your host.

It is set to 25 by default, but you can change it depending on the SMTP settings of your email hosting domain.

 ![10.png](configuring-email-notifications-through-cluster-settings.assets/10.png)

 

1. Next, you can specify the maximum size limit for your email attachments. This, too, depends on the size limit allowed by your host. For instance, by default, GMAIL does not send emails that exceed 25MB. If the attachment exceeds the limit, you will get an error.

![11.png](configuring-email-notifications-through-cluster-settings.assets/11.png)

 

1. The next option is the *Use SSL* checkbox. Choosing this option will create a security layer to encrypt your emails, so they can’t be read at any point between your system and the mail server. It is recommended to check this option to make sure your email notifications are sent and received in a secure environment.

 ![12.png](configuring-email-notifications-through-cluster-settings.assets/12.png)

 

1. The final step is to provide the User Credentials, that is, the login information of the account the mail notifications will be sent from. Type in the User ID in the *Logon* field, and password in the *Password* field. 

![13.png](configuring-email-notifications-through-cluster-settings.assets/13.png)

If you have set up email notifications in the Cluster Settings previously, you can check the ‘*Use default credentials’* checkbox to use the credentials you have used in the past. However, if you're using this setup for the first time, you will have to provide the login ID and password.

![14.png](configuring-email-notifications-through-cluster-settings.assets/14.png)

 

1. Click Save to save this mail setup on your server.

![15.png](configuring-email-notifications-through-cluster-settings.assets/15.png)

 

1. To check if your email has been set up successfully, you can send a test mail using the *Send Test Email* option.

 ![16.png](configuring-email-notifications-through-cluster-settings.assets/16.png)

 

1. When you click on the ‘Send Test Email’ button, a new Test Email window will open on the screen.

![17.png](configuring-email-notifications-through-cluster-settings.assets/17.png)

By default, the sender and recipient IDs are set to the Sender ID you provided in the previous steps. But if you want to change the recipient or send a test email to more than one recipient, you can do that by adding email addresses in the recipient field.

Same is the case with the Subject and Body of the test mail. You can modify the default Subject and Body settings of the test mail.

 ![18.png](configuring-email-notifications-through-cluster-settings.assets/18.png)

 

1. When you’re done, click Send. It will take a few seconds to run the test mail, and when the test mail has been sent successfully, this message box will pop up on the screen.

 ![20.png](configuring-email-notifications-through-cluster-settings.assets/20.png)

 

 

This concludes configuring email notifications through Server Cluster Settings. Leave a thumbs up if you found this article helpful. If you have any queries, feel free to reach out by dropping a comment below, and we will get back to you. 