# Setting Up Oracle ODP.Net Connectivity in Centerprise

To connect to an Oracle database using ODP.Net drivers, you need to have the appropriate Oracle driver installed.  This article explains the steps needed to install the driver.

\1. If the ODP.Net driver is not already installed on the target machine, follow the steps below to install the driver.

\2. Visit the Oracle website *www.oracle.com* and navigate to the **32-bit/64-bit Oracle Data Access Components (ODAC)** downloads page.

**Note**:  You may use the links below.  These links are provided for your convenience.  They are working as of the time of writing this article.  However, please note that Oracle may change their website structure in the future.

For installing the 32-bit driver:

http://www.oracle.com/technetwork/topics/dotnet/utilsoft-086879.html

For installing the 64-bit driver:

https://www.oracle.com/technetwork/database/windows/downloads/index-090165.html

**Note:** We have shown the steps to install the 32-bit driver in this article. The same steps will be followed to install a 64-bit driver.

1.  Accept the Terms and Conditions and select the *ODTwithODAC121024.zip*archive.

 ![oracle1.png](setting-up-oracle-odp.net-connectivity-in-centerprise.assets/oracle1.png)

**Note**: This archive has the 12c version of ODAC, which has been verified to work with Centerprise.  Optionally, you may download an earlier version of the driver, such as 11.2, which has been verified to work with Centerprise as well.

1. Unpack the contents of the archive downloaded in the previous step, and run *Setup.exe.*

**Important Note**:  It is strongly recommended that you run the Setup under an Administrator account.   One of the ways to accomplish this is to right-click the executable and select **Run as Admin**.

1. This opens the Oracle Universal Installer. Keep the default selections in Step 1 through Step 3. In Step 4, select to install **Oracle Data Provider for .NET**, as shown in the screenshot below.

 ![oracle2.png](setting-up-oracle-odp.net-connectivity-in-centerprise.assets/oracle2.png)

 

1. In Step 5 and Step 6, keep the default selections as shown in the screenshots below (if your environment requires setting up TNSNAMES.ORA file, you will need to set the values accordingly.  Please contact your Oracle DBA for details).

 

 ![oracle3.png](setting-up-oracle-odp.net-connectivity-in-centerprise.assets/oracle3.png)

![oracle4.png](setting-up-oracle-odp.net-connectivity-in-centerprise.assets/oracle4.png)

 

1. Finally, confirm the desired configuration as shown below, and click Install:

 ![oracle5.png](setting-up-oracle-odp.net-connectivity-in-centerprise.assets/oracle5.png)

 

This installs the ODP.Net drivers.

It is recommended that you restart the OS after the installation is complete.

1. To test ODP.Net connectivity in Centerprise, create a new dataflow and add a database source object. In the list of providers, select **Oracle ODP.Net** as shown below:

 ![oracle6.png](setting-up-oracle-odp.net-connectivity-in-centerprise.assets/oracle6.png)

 

 

Enter the connection information for your target Oracle database, and click **Test Connection…** to verify that the connection is successful:

 ![oracle7.png](setting-up-oracle-odp.net-connectivity-in-centerprise.assets/oracle7.png)