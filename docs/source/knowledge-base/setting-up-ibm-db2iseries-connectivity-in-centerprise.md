# Setting Up IBM DB2/iSeries Connectivity in Centerprise

To connect to a DB2 or iSeries database using the IBM DB2 driver, you need to have the appropriate driver installed on the host where Centerprise client and/or server runs.  This article explains the steps needed to install the driver.

If the DB2 driver is not already installed on the target machine, please follow the steps below to have it installed.

- Visit the IBM website at *ibm.com* and navigate to the IBM support page.

**Note**:  You may use the link below.  This link is provided for your convenience.  It is working as of the time of writing this article.  However, please note that IBM may change their website structure in the future.

http://www-01.ibm.com/support/docview.wss?uid=swg21385217

- Click the **IBM Data Server Driver Package (DS Driver)** link as shown below:

 ![img](setting-up-ibm-db2iseries-connectivity-in-centerprise.assets/fad37856333bcdffadb97e8dcd61ef343f7bcaf45b42c3608a1838877b6b9e38.png)

 

- Select the appropriate driver for your target OS, keeping in mind that you will need 32 bit driver for 32 bit OS, and 64 bit driver for 64 bit OS. The two most appropriate choices are highlighted in red below.

 ![img](setting-up-ibm-db2iseries-connectivity-in-centerprise.assets/cfed8ead7463dcce751bd22a1050d00cddd30e3c12a2287285e22f730c91037c.png)

 

- You may be required to sign in. If you do not have IBM universal user account, you will need to create it before proceeding to the next step.
- In the next page, make the appropriate selections, then agree to the license terms, and click **I Confirm**.

![img](setting-up-ibm-db2iseries-connectivity-in-centerprise.assets/bc8cf23185495e658eb15b34b751cc8a00f4093d88c83278c63d4c14c881e757.png)

 

- Make sure that **IBM Data Server Driver Package** is selected and click **Download Now**. This will download the driver installer. 

![img](setting-up-ibm-db2iseries-connectivity-in-centerprise.assets/0566356ea6bd1333f52dd3004e43c109e6b370dd169dd47132a4adca0c7ba644.png)

**Note**: In the example above, we downloaded version 11.1 of IBM DB2 driver, which has been verified to work with Centerprise.  Optionally, you may use an earlier version of the driver, such as 10.1 or 9.7, which have been verified to work with Centerprise as well.

- Run the installer file*.*

**Note**:  It is strongly recommended that you run the installer under an Administrator account.   One of the ways to accomplish this is to right-click the executable and select **Run as Admin**.

- This opens the DB2 Installer. Keep the default selections throughout the installer, as shown in the screenshots below.

 ![img](setting-up-ibm-db2iseries-connectivity-in-centerprise.assets/8d142b71fdd1863ebec9e4a5db3a14f13c40c23b984d8f73dfec0144babfc40f.png)

![img](setting-up-ibm-db2iseries-connectivity-in-centerprise.assets/f81ab6d1aac4f76aaabe37c72cbec49eeb6f8d2c4502200b6ddcf6141fcc1164.png)

 ![img](setting-up-ibm-db2iseries-connectivity-in-centerprise.assets/b6947e274fca196574f4f3a2da1bd7ff611694860671aa81d9f342e2c93a4cbf.png)

 

 ![img](setting-up-ibm-db2iseries-connectivity-in-centerprise.assets/c44fba76931533ee91999257bd2823c1a2c81ca9d4837c755c98a009f6e9729e.png)

- Finally, confirm the desired configuration as shown below, and click **Install**:

 ![img](setting-up-ibm-db2iseries-connectivity-in-centerprise.assets/885685146f417c3049793b2a92b72e8ac9453bb049cfb5208d4c0bd4047d35b8.png)

 

- This installs the IBM DB2 driver.

It is recommended that you restart the OS after the installation is complete.

![img](setting-up-ibm-db2iseries-connectivity-in-centerprise.assets/895cc377dc589053b8b18f6aa5a81cdc831e9d1c970e683149bf009ef763cb19.png)

- To test IBM DB2 connectivity in Centerprise, create a new dataflow and add a database source object. In the list of providers, select **DB2** as shown below:

 ![img](setting-up-ibm-db2iseries-connectivity-in-centerprise.assets/07562fbe9f149281e60c524b210422de1fb828d10a0be5a30eefb338f537215f.png)

- Enter the connection information for your target DB2 or iSeries database, and click **Test Connection…** to verify that the connection is successful:

![img](setting-up-ibm-db2iseries-connectivity-in-centerprise.assets/c9a16d9116cb10a2da160305e7f3ee29968c4dc3d62290557bd760b94c9f8a20.png)

 