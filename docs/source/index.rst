.. RTD-Sphinx documentation master file, created by
   sphinx-quickstart on Fri Jul  5 12:15:22 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Astera's documentation!
======================================

* `Centerprise Documentation </projects/centerprise>`_
* `ReportMiner Documentation </projects/reportminer>`_
* `EDIConnect Documentation </projects/ediconnect>`_
* `Data Virtualization Documentation </projects/data-virtualization>`_

.. toctree::
   :maxdepth: 1
   :caption: Knowledgebase
   :name: knowledgebase-toc
   
   /knowledge-base/commonly-asked-questions-about-cardinality-errors.md
   /knowledge-base/configuring-email-notifications-through-cluster-settings.md
   /knowledge-base/connecting-to-amazon-sftp-to-access-data-from-an-aws-bucket.md
   /knowledge-base/connecting-to-source-control.md
   /knowledge-base/how-to-connect-to-eloqua-using-centerprise-rest-api.md
   /knowledge-base/how-to-parse-information-in-a-field-using-asteras-builtin-functions.md
   /knowledge-base/installing-centerprise-data-integrator.md
   /knowledge-base/installing-centerprise.md
   /knowledge-base/oracle-client-tools-setup.md
   /knowledge-base/oracle-data-load-options-in-centerprise-7.4.md
   /knowledge-base/overview-of-cardinality-in-data-modeling.md
   /knowledge-base/parameterization.md
   /knowledge-base/posting-data-using-the-rest-client.md
   /knowledge-base/project-deployment.md
   /knowledge-base/release-notes-for-centerprise-7.1.md
   /knowledge-base/replacing-licenses-in-centerprise.md
   /knowledge-base/replacing-licenses-in-reportminer.md
   /knowledge-base/reportminer-installation-guide.md
   /knowledge-base/reportminer-ocr-best-practices.md
   /knowledge-base/running-microsoft-access-database-engine-with-centerprise.md
   /knowledge-base/setting-up-astera-integration-server-7.md
   /knowledge-base/setting-up-ibm-db2iseries-connectivity-in-centerprise.md
   /knowledge-base/setting-up-oracle-odp.net-connectivity-in-centerprise.md
   /knowledge-base/setting-up-the-server.md
   /knowledge-base/system-requirements.md
   /knowledge-base/upgrading-centerprise-to-version-7.37.4.md
   /knowledge-base/upgrading-from-centerprise-5.1-to-6.0.md
   /knowledge-base/using-the-rest-client-to-download-a-text-file.md
   /knowledge-base/what-is-new-in-centerprise-7.4.md
   /knowledge-base/whats-new-in-centerprise-7.4.1.221.md